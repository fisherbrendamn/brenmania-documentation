## Sauvegarder ses bases de donnees postgresql

On commence par creer une base de donnees de test : 
```sql
createdb madb
```

Puis on va venir ajouter des donnees dans cette base : 
```sql
psql -d madb -c "CREATE TABLE tb1 (id int)"
psql -d madb -c "CREATE TABLE tb2 (id int)"
psql -d madb -c "insert into tb1 select * from GENERATE_SERIES(1,1000)"
psql -d madb -c "insert into tb2 select * from GENERATE_SERIES(1,1000)"
```

Enfin, on vient faire une savuegarde de la base, grace au binaire **pg_dump** (l'option `-d` permet de specifier la database a dumper) :

```sql
pg_dump -d madb > madb-test.sql
```

Rq: par default, le dump est au format plain text. Nous verrons le format specifique de posqtgres ensuite ...

```bash
psql -l # Permet de voir la liste des bases de donnees sur notre instance
```




