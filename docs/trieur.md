# Trieur de fichier

Ce code permet de trier des fichiers dans un répertoire en fonction de leur extension.
**Python3.9**

```python
import os
from glob import glob
import shutil

le_path = "/Users/hmaldant/Documents/sandbox/apprentissage/udemy/section35_dictionnaires/01-sources/tri_fichiers_sources/"
chemin = os.path.join(le_path)

extensions = {".mp3": "Musique",
              ".wav": "Musique",
              ".mp4": "Videos",
              ".mov": "Videos",
              ".jpeg": "Images",
              ".jpg": "Images",
              ".png": "Images",
              ".pdf": "Documents"}

fichiers = glob(os.path.join(chemin, "*"))
for fichier in fichiers:
    extension = os.path.splitext(fichier)[-1]
    dossier = extensions.get(extension)
    if dossier:
        chemin_dossier = os.path.join(chemin, dossier)
        os.makedirs(chemin_dossier, exist_ok=True)
        shutil.move(fichier, chemin_dossier)
```
