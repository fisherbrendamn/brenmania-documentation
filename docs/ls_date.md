# Sort 'ls' output by date

Pour trier ses fichiers par date, par exemple dans un dossier /Downloads ou / Telechargement volumineux, on peut utiliser l'argument `-t` de **ls**. 
Ajouter a cela un pipe suivi de la `head`pour avoir un affichage propre des resultats les plus recents : 

```bash
ls -laht | head
```

