# sed : le grand remplacement

Le comportement de sed est différent si l'on se trouve sur Linux ou sur MacOS.

Sur Linux :

```bash
sed -i 's/==/>=/g' requirements.txt
```
Sur MacOS :

```bash
sed -i .old 's/==/>=/g' requirements.txt
```
