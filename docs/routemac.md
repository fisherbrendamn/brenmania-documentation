# Routing sur MacOS

Pour ajouter une route, on peut utiliser la methode classique avec : 

```bash
sudo route -n add -net <IP> 192.168.1.1
sudo route -n add -net <IP> 192.168.1.1
```

Mais il existe un utilitaire sur Mac, du nom de **networksetup** qui permet de jouer avec la partie reseau.

## lister les routes

En plus de lister les routes avec `netstat -rn`, on peut utiliser `sudo networksetup -getadditionalroutes <service reseau>` (`sudo -listallnetworkservices` pour lister les services reseau).

## Routage permanent - Ajouter des routes permanentes

On peut ensuite ajouter une route :
```bash
sudo networksetup -setadditionalroutes Ethernet 192.168.1.0 255.255.255.0 10.0.0.2
```

